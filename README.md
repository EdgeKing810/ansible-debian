# Debian Ansible Test Image

Debian Docker containers for Ansible playbook and role testing.

## How to Use

  1. [Install Docker](https://docs.docker.com/engine/installation/).
  2. Pull this image from Ninux Gitlab Docker Registry:
  
  ```bash
    # Debian 8
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-debian:8
    
    # Debian 9
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-debian:9
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-debian:latest

    # Debian 10
    docker pull registry.gitlab.com/ninuxorg/docker/ansible-debian:10
  ```
   
  3. Run a container from the image:
    
  ```bash
    # Debian 8
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-debian:8 /lib/systemd/systemd
    
    # Debian 9
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-debian:9 /lib/systemd/systemd
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-debian:latest /lib/systemd/systemd

     # Debian 10
    docker run --detach --privileged --volume=/sys/fs/cgroup:/sys/fs/cgroup:ro registry.gitlab.com/ninuxorg/docker/ansible-debian:10 /lib/systemd/systemd
  ```

  4. Use Ansible inside the container:
    
  ```bash
    docker exec --tty [container_id] env TERM=xterm ansible --version
    docker exec --tty [container_id] env TERM=xterm ansible-playbook /path/to/ansible/playbook.yml --syntax-check
  ```
